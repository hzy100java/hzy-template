package com.template.mapper;

import com.template.entity.User;

/**
 * @Author huangzhaoyong
 * @ClassName UserMapper
 * @Description TODO
 * @Date 2018/8/3 17:55
 * @Version 1.0
 */
public interface UserMapper {

    User selectOne();
}
