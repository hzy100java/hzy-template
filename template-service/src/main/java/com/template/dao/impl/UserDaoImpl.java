package com.template.dao.impl;

import com.template.entity.User;
import com.template.mapper.UserMapper;
import com.template.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author huangzhaoyong
 * @ClassName UserDaoImpl
 * @Description TODO
 * @Date 2018/8/3 17:57
 * @Version 1.0
 */
@Service
public class UserDaoImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public User selectOne() {
        return userMapper.selectOne();
    }
}
