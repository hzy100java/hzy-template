package com.template.dao;

import com.template.entity.User;

/**
 * @Author huangzhaoyong
 * @ClassName UserDao
 * @Description TODO
 * @Date 2018/8/3 17:57
 * @Version 1.0
 */
public interface UserDao {

    User selectOne();
}
