package com.template.service;

import com.template.entity.User;

/**
 * @Author huangzhaoyong
 * @ClassName UserService
 * @Description TODO
 * @Date 2018/8/3 17:53
 * @Version 1.0
 */
public interface UserService {

    User selectOne();
}
