package com.template.service.impl;

import com.template.dao.UserDao;
import com.template.entity.User;
import com.template.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author huangzhaoyong
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Date 2018/8/3 17:55
 * @Version 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public User selectOne() {
        return userDao.selectOne();
    }
}
