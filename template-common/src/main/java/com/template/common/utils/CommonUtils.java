package com.template.common.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author huangzhaoyong
 * @ClassName Common
 * @Description TODO
 * @Date 2018/7/11 14:54
 * @Version 1.0
 */
public class CommonUtils {

    public static<T> Map<String, List<T>> listToMapList(List<T> list, String key) throws Exception{
        Map<String, List<T>>  result = new HashMap<>();
        List<T> tList;
        Class cls;
        Field[] fields;
        String value;
        for(T t: list){
            tList = new ArrayList<>();
            cls = t.getClass();
            //得到所有属性
            fields = cls.getDeclaredFields();
            for(Field field: fields){
                //打开私有访问
                field.setAccessible(true);
                //获取属性
                String name = field.getName();
                if(!name.equals(key) || null == field.get(t)){
                    continue;
                }
                value = field.get(t).toString();
                if(null != result.get(value)){
                    tList = result.get(value);
                }
                tList.add(t);
                result.put(value, tList);
            }
        }
        return result;
    }

    public static<T> Map<String, T> listToMap(List<T> list, String key) throws Exception{
        Map<String, T>  result = new HashMap<>();
        Class cls;
        Field[] fields;
        String value;
        for(T t: list){
            cls = t.getClass();
            //得到所有属性
            fields = cls.getDeclaredFields();
            for(Field field: fields){
                //打开私有访问
                field.setAccessible(true);
                //获取属性
                String name = field.getName();
                if(!name.equals(key) || null == field.get(t)){
                    continue;
                }
                value = field.get(t).toString();
                result.put(value, t);
            }
        }
        return result;
    }
}
