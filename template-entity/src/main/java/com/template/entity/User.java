package com.template.entity;

import lombok.Data;

/**
 * @Author huangzhaoyong
 * @ClassName User
 * @Description TODO
 * @Date 2018/8/3 16:58
 * @Version 1.0
 */
@Data
public class User {
    private String userId;
    private String userName;
    private String password;
}
